# CI Charts

Charts targeted towards development and testing that are deployable by Catalyst.

## mediawiki

---
## !!! *Important* !!!

When using a repository cache/pool (see below), this chart will create worktrees in the repositories
for performance reasons. This requires writing to the `.git` directory, which means the pool is
mounted in write mode.

If you modify this chart, please be mindful about this fact and make sure you don't add changes to
the repos that may impact other consumers of the pool.

---

This chart deploys a Mediawiki instance.

The chart contains an `initContainer` that will install Mediawiki. Should this process fail, the
init container will be restarted and left running indefinitely to allow for troubleshooting. Logs
and exit codes from previous runs will be stored at `/var/www/html`.

### Caches

If you are deploying this chart to a local cluster on your machine during development, you can
use `caches.wikiRepos` to point to a location on your disk containing wiki repositories. They need
to follow the same structure Patchdemo uses, with the repository at the target directory
where Patchdemo clones repos on initialization. The chart will use those repos as references
when cloning and save a significant amount of time and space.

You can also specify `caches.build` to point to an empty local path. When creating a new environment,
the chart will reuse that location to place artifacts created during the different build steps, in
particular `npm` and `composer` packages that the chart downloads.

## License

GNU AFFERO GENERAL PUBLIC LICENSE