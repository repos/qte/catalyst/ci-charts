#!/bin/bash
set -exu -o pipefail

log 'Running composer'

export COMPOSER_ALLOW_SUPERUSER=1
if [ -d /srv/build-cache ]; then
  export COMPOSER_CACHE_DIR=/srv/build-cache/composer
fi

cd "$MW_HOME"
# Core
composer install --no-dev

while read -r REPO; do
  if [ -f "$MW_HOME"/"$REPO"/composer.json ]; then
  	cd "$MW_HOME"/"$REPO"
  	composer install --no-dev
  fi
done <<<"$(grep -v \# "$FILES"/composer/composerinstall)"
