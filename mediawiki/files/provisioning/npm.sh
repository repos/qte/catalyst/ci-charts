#!/bin/bash
set -exu -o pipefail

log 'Setting up npm'

cd "$BASE"
cp -rL "$FILES"/npm/* .
if [ -d /srv/build-cache ]; then
  export npm_config_cache=/srv/build-cache/npm
fi

npm ci --omit=dev
# Let www-data run NPM
chown -R www-data: node_modules
