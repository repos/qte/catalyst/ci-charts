{{ define "mediawiki.bash_helpers" }}
            function log {
                (set +x; echo "############### $1 ###############")
            }
            function error {
                (set +x; echo "############### ERROR: $1")
                exit 1
            }

            function fetch_branch {
                REPO=$1
                BRANCH=$2

                # If the branch doesn't exist for the repo, fallback to the mainline (which can be
                # "master" or "main")
                if ! git -C $REPO fetch origin $BRANCH; then
                    if ! git -C $REPO fetch origin master; then
                        if ! git -C $REPO fetch origin main; then
                            error 'Repo has neither "master" nor "main" branch. That should not happen'
                        fi
                    fi
                fi
            }

            function checkout_branch {
                REPO=$1
                BRANCH=$2

                fetch_branch $REPO $BRANCH
                git -C $REPO checkout FETCH_HEAD
            }

            function add_worktree {
                REPO=$1
                TARGET=$2
                BRANCH=$3

                fetch_branch $REPO $BRANCH

                # If there's a bug somewhere and we end up creating worktrees with shared paths for
                # different environments (note the info is stored in the $REPO which is part of the
                # repo pool and shared across envs), running the `prune` command helps us recover
                # from that situation
                git --git-dir=$REPO/.git worktree prune
                # The git $WT needs to be a unique path *across* envs, because metadata is stored in
                # the shared pool $REPO and worktrees are identified by their paths
                WT=$TARGET/{{ .Release.Name }}-$(date +%s)
                git --git-dir=$REPO/.git worktree add --detach $WT FETCH_HEAD
                shopt -s dotglob
                mv $WT/* $TARGET
                shopt -u dotglob
                rm -rf $WT
            }

            export -f log error fetch_branch checkout_branch add_worktree
{{ end }}

{{- define "db.getRootPassword" -}}
{{- $secretName := printf "%s-db-secret" (.Release.Name ) -}}
{{- $existingSecret := lookup "v1" "Secret" .Release.Namespace $secretName | default dict -}}

{{- /* If the secret already exists and has a data key, reuse it. */ -}}
{{- if $existingSecret.data -}}
{{- if (index $existingSecret.data "root-password") -}}
{{- index $existingSecret.data "root-password" -}}
{{- end -}}

{{- /* Else if user provided a plain-text value in .Values.mySecretValue, base64-encode it. */ -}}
{{- else if .Values.db.rootPassword -}}
{{ .Values.db.rootPassword | b64enc }}

{{- /* Otherwise generate a random 16-char string and base64-encode it. */ -}}
{{- else -}}
{{ randAlphaNum 16 | b64enc }}
{{- end -}}
{{- end -}}

{{- define "db.default_rootPassword" -}}
{{ if .Values.db.rootPassword }} "{{ .Values.db.rootPassword }}" {{ else }} {{ randAlphaNum 20 | b64enc | quote }} {{ end }}
{{- end -}}

{{- define "mediawiki.default_ingress" -}}
{{- .Values.mediawikiCore.ingress | default (print .Release.Name ".catalyst.wmcloud.org") -}}
{{- end -}}

{{- define "mediawiki.get_core" }}
            # MW core needs to be at /var/www/html/w, otherwise things like the apache config
            # (which we have no control over) won't work
            TARGET=$MW_HOME
            if [ -n "$REF_REPOS" ]; then
                add_worktree $REF_REPOS/mediawiki/core $TARGET {{ .branch }}
            else
                git clone --single-branch $GERRIT_URL/mediawiki/core $TARGET
                checkout_branch $TARGET {{ .branch }}
            fi
    {{- if .patches }}
        {{- range $patch := .patches }}
            REPO=$TARGET REF={{ $patch.ref }} \
            HASH={{ $patch.hash }} BASE={{ $patch.base }} \
            apply_patch.sh
        {{- end }}
    {{- end }}
{{ end }}

{{- define "mediawiki.get_extensions" -}}
{{- include "mediawiki.get_repos_in_batches" (dict "type" "ext" "repos" .) }}
{{- end }}

{{- define "mediawiki.get_skins" -}}
{{- include "mediawiki.get_repos_in_batches" (dict "type" "skin" "repos" .) }}
{{- end }}

{{- define "mediawiki.get_modules" -}}
{{- include "mediawiki.get_repos_in_batches" (dict "type" "mod" "repos" .) }}
{{- end }}

{{- define "mediawiki.get_repos_in_batches" -}}
{{- $type := .type -}}
{{- $enabledRepos := list -}}
{{- range $repo := .repos -}}
    {{- if $repo.enable -}}
        {{- $enabledRepos = append $enabledRepos $repo.name }}
        {{- $params := dict "baseFunName" (printf "checkout_%s" $type) "name" $repo.name "values" $repo -}}
        {{- if eq $type "ext" -}}
            {{- include "mediawiki.get_extension_function" $params }}
        {{- else if eq $type "skin" -}}
            {{- include "mediawiki.get_skin_function" $params }}
        {{- else if eq $type "mod" -}}
            {{- include "mediawiki.get_module_function" $params }}
        {{- end -}}
    {{ end -}}
{{- end -}}

{{- $batchSize := 5 -}}
{{- $reposNr := (len $enabledRepos) -}}
{{/* The total number of batches will be: $reposNr / $batchSize + 1 batch *if* there's a remainder*/}}
{{- $batchesNr := add (div $reposNr $batchSize) (min (mod $reposNr $batchSize) 1) -}}
{{- range $_ := until (int $batchesNr) -}}
    {{- $chunkSize := (min $batchSize (len $enabledRepos)) -}}
    {{- $batch := slice $enabledRepos 0 $chunkSize -}}
    {{- range $name := $batch }}
            checkout_{{ $type }}_{{ $name }}&
    {{- end }}
            wait
    {{- $enabledRepos = slice $enabledRepos $chunkSize -}}
{{- end -}}
{{- end }}

{{- define "mediawiki.get_extension_function" }}
            function {{ .baseFunName }}_{{ .name }} {
                NAME={{ .name }}
                TARGET=$MW_HOME/extensions/$NAME
                if [ -n "$REF_REPOS" ]; then
                    add_worktree $REF_REPOS/mediawiki/extensions/$NAME $TARGET {{ .values.branch }}
                else
                    git clone --single-branch $GERRIT_URL/mediawiki/extensions/$NAME $TARGET
                    checkout_branch $TARGET {{ .values.branch }}
                fi
    {{ if .values.patches -}}
        {{- range $patch := .values.patches }}
                REPO=$TARGET REF={{ $patch.ref }} \
                HASH={{ $patch.hash }} BASE={{ $patch.base }} \
                apply_patch.sh
        {{- end -}}
    {{- end }}
            }
{{ end }}

{{- define "mediawiki.get_skin_function" }}
            function {{ .baseFunName }}_{{ .name }} {
                NAME={{ .name }}
                TARGET=$MW_HOME/skins/$NAME
                if [ -n "$REF_REPOS" ]; then
                    add_worktree $REF_REPOS/mediawiki/skins/$NAME $TARGET {{ .values.branch }}
                else
                    git clone --single-branch $GERRIT_URL/mediawiki/skins/$NAME $TARGET
                    checkout_branch $TARGET {{ .values.branch }}
                fi
    {{ if .values.patches -}}
        {{- range $patch := .values.patches }}
                REPO=$TARGET REF={{ $patch.ref }} \
                HASH={{ $patch.hash }} BASE={{ $patch.base }} \
                apply_patch.sh
        {{- end }}
    {{- end }}
            }
{{ end }}

{{- define "mediawiki.get_module_function" }}
            function {{ .baseFunName }}_{{ .name }} {
                NAME={{ .name }}
                MODULE_INFO=$(grep $NAME $FILES/modules/modules)
                M_SOURCE=$(echo $MODULE_INFO | cut -f2 -d\ )
                M_TARGET=$(echo $MODULE_INFO | cut -f3 -d\ )
                TARGET=$MW_HOME/$M_TARGET
                if [ -n "$REF_REPOS" ]; then
                    add_worktree $REF_REPOS/$M_SOURCE $TARGET {{ .values.branch }}
                else
                    git clone --single-branch $GERRIT_URL/$M_SOURCE $TARGET
                    checkout_branch $TARGET {{ .values.branch }}
                fi
    {{ if .values.patches -}}
        {{- range $patch := .values.patches }}
                REPO=$TARGET REF={{ $patch.ref }} \
                HASH={{ $patch.hash }} BASE={{ $patch.base }} \
                apply_patch.sh
        {{- end }}
    {{- end }}
            }
{{ end }}
